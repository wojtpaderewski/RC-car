from machine import PWM, Pin

class Servo:
    pin = PWM(Pin(2))
    
    def __init__(self):
        self.pin.freq(50)
    
    def moveServo(self, joystickAngle):
        magic_value = 65535
        value = joystickAngle * magic_value
        self.pin.duty_u16(value)