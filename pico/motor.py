from machine import Pin, PWM

class Motor:
    motorSpeedPin = PWM(Pin(0))
    AInputPin = Pin(0)
    BInputPin = Pin(0)
    
    def __init__(self, speedPin, AInput, BInput):
        self.motorSpeedPin = PWM(Pin(speedPin))
        self.motorSpeedPin.freq(1000)
        self.AInput = Pin(AInput)
        self.BInput = Pin(BInput)
        
    def move(self, joystickSpeed):
        if joystickSpeed > 0:
           self.moveForward(joystickSpeed)
        if joystickSpeed < 0:
            self.moveBackward(joystickSpeed)
        if joystickSpeed == 0:
            self.stop()
            
    def moveForward(self, speed):
        onePrecentValue = 650.25
        self.motorSpeedPin.duty_u16(onePrecentValue * speed)
        self.AInput.high()
        self.BInput.low()
        
    def moveBackward(self, speed):
        onePrecentValue = 650.25
        self.motorSpeedPin.duty_u16(onePrecentValue * speed)
        self.AInput.low()
        self.BInput.high()
        
    def stop(self):
        self.motorSpeedPin.duty_u16(0)
        self.AInput.low()
        self.BInput.low()
        