from machine import Pin,PWM,UART
import json
import sys

class Motor:
    motorSpeedPin = PWM(Pin(0))
    AInputPin = Pin(0)
    BInputPin = Pin(0)
    
    def __init__(self, speedPin, AInput, BInput):
        self.motorSpeedPin = PWM(Pin(speedPin))
        self.motorSpeedPin.freq(1000)
        self.AInput = Pin(AInput)
        self.BInput = Pin(BInput)
        
    def move(self, joystickSpeed):
        if joystickSpeed > 0:
           self.moveForward(joystickSpeed)
        if joystickSpeed < 0:
            self.moveBackward(joystickSpeed)
        if joystickSpeed == 0:
            self.stop()
            
    def moveForward(self, speed):
        onePrecentValue = 650.25
        self.motorSpeedPin.duty_u16(onePrecentValue * speed)
        self.AInput.high()
        self.BInput.low()
        
    def moveBackward(self, speed):
        onePrecentValue = 650.25
        self.motorSpeedPin.duty_u16(onePrecentValue * speed)
        self.AInput.low()
        self.BInput.high()
        
    def stop(self):
        self.motorSpeedPin.duty_u16(0)
        self.AInput.low()
        self.BInput.low()

uart = UART(0, baudrate = 115200, tx = Pin(0), rx = Pin(1))
leftMotor = Motor(2,3,4)
rightMotor = Motor(5,6,7)

def writeUARTDataToArray(rxdata):
    data = bytes()
    for d in rxdata:
        data += d
    return data

while True:
    rxdata = []
    while uart.any() > 0:
        rxdata.append(uart.read(1))
        print('')
        
    data = writeUARTDataToArray(rxdata)
    if len(data) > 0:
        try:
            uart_data = data.decode()
            Json = json.loads(uart_data)
            speed = Json['speed']
            angle = Json['angle']
            leftMotor.move(speed)
            rightMotor.move(speed)
            print('speed: ', speed)
            print('angle: ', angle)
        except IndexError: 
            print("Error decoding")