import {
    Body,
    Controller,
    Get,
    Path,
    Post,
    Query,
    Route,
    SuccessResponse,
} from 'tsoa';
import { esp8266 } from '../ESP/Esp8266';

interface Speed {
    speed: number;
    angle: number;
}

@Route('/joystick')
export class JoystickController extends Controller {
    @Post()
    public async setSpeed(@Body() position: Speed): Promise<void> {
        try {
            esp8266.hendelJoystick(position.speed, position.angle);
        } catch(err) {
            console.log(err);
        }
    }
}