import axios, { AxiosInstance } from 'axios'

class Esp8266 {
    api: AxiosInstance;
    
    constructor (ip: string) {
        this.api = axios.create({
            baseURL: `http://${ip}:80/`,
            withCredentials: false,
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            }
        });
    }

    async hendelJoystick(speed: number, angle: number) : Promise<void> {
        try {
            console.log(speed, angle);
            await this.api.post('joy/',{ speed, angle });
        } catch (error) {
            console.log(error);
        }
    }
}

export const esp8266 = new Esp8266('192.168.0.186');
