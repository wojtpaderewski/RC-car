import express from 'express';
import http from 'http';
import { logger } from './logger';
import { requestLoggerMiddleware } from './request.logger.middleware';
import { RegisterRoutes } from './routes';
import path from 'path';

const app = express();
const server = http.createServer(app);

const port = 4000;
const ip = '192.168.0.177';

app.use(express.static('public'));
app.use(express.json());

app.use(requestLoggerMiddleware);
RegisterRoutes(app);

server.listen(port, ip, () => {
    logger.info(`app started at http://${ip}:${port}`);
});

app.use('/swagger.json', (req, res) => {
    res.sendFile(path.join(__dirname, '..', 'swagger.json'));
});

