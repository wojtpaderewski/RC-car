module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es2021: true
  },
  ignorePatterns: ["**/*.js"],
  extends: [
    'plugin:@typescript-eslint/recommended'
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 12
  },
  plugins: [
    '@typescript-eslint'
  ],
  rules: {
    '@typescript-eslint/triple-slash-reference': 0,
    '@typescript-eslint/no-unused-vars': 0,
    "indent": [
        "error",
        4,
        { "SwitchCase": 1 }
    ],
    "quotes": [
        "error",
        "single",
        {
            "allowTemplateLiterals": true
        }
    ],
    "semi": [
        "error",
        "always"
    ],
    "no-unused-vars": 0,
    "no-var": [
        "error"
    ]
  }
}